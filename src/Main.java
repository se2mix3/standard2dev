import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

import javax.swing.text.html.ImageView;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main extends Application {

    private static Connection conn = null;
    private static String driver = "oracle.jdbc.driver.OracleDriver"; //驱动
    private static String url = "jdbc:oracle:thin:@//20.10.130.204:1521/orcl"; //连接字符串
    private static String username = ""; //用户名
    private static String password = "1"; //密码


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create the custom dialog.
        Dialog<Map<String,String>> dialog = new Dialog<>();
        dialog.setTitle("SQL EXPORT");
        dialog.setHeaderText("估计有bug，但是不给改。");

// Set the icon (must be included in the project).

// Set the button types.
        ButtonType loginButtonType = new ButtonType("导出", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);


// Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField jzk = new TextField();
        jzk.setPromptText("NCC2004STANDARD");
        jzk.setText("NCC2004STANDARD");
        TextField dev = new TextField();
        dev.setPromptText("NCC20041126");
        dev.setText("NCC20041126");
        TextField appcode = new TextField();
        appcode.setPromptText("10140BOMM");
        appcode.setText("10140BOMM");
        StringBuilder log = new StringBuilder();
        TextArea textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);

        grid.add(new Label("基准库数据源:"), 0, 0);
        grid.add(jzk, 1, 0);
        grid.add(new Label("开发库数据源:"), 0, 1);
        grid.add(dev, 1, 1);
        grid.add(new Label("APPCODE:"), 0, 2);
        grid.add(appcode, 1, 2);
        grid.add(textArea,1,3);

// Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        //loginButton.setDisable(true);

// Do some validation (using the Java 8 lambda syntax).
        jzk.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });
        dev.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });
        appcode.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
        Platform.runLater(() -> jzk.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                Map<String,String> result = new HashMap<>();
                result.put("standard",jzk.getText());
                result.put("dev",dev.getText());
                result.put("appcode",appcode.getText());

                return result;
            }else{
                System.exit(0);
            }
            return null;
        });

        Optional<Map<String, String>> result = dialog.showAndWait();
        while(result!=null){
            result.ifPresent(data->{
                String devds = data.get("dev");
                String jzkds = data.get("standard");
                String code = data.get("appcode");
                username=jzkds;
                PreparedStatement pstmt = null;


                //处理appregister
                try {
                    textArea.appendText("开始处理app_register"+System.lineSeparator());

                    //textArea.setText(log.toString());
                    String querypage = "select * from sm_appregister where code='"+code+"' and dr=0";
                    pstmt = getConn().prepareStatement(querypage);
                    textArea.appendText("\t"+querypage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    ResultSet rs = pstmt.executeQuery();
                    String pk_appregister = null;
                    while (rs.next()) {
                        pk_appregister= rs.getString("pk_appregister");
                        System.out.println(pk_appregister);
                    }
                    String deletepage = "delete from "+devds+".sm_apppage where parent_id='"+pk_appregister+"'";
                    pstmt = getConn().prepareStatement(deletepage);
                    textArea.appendText("\t"+deletepage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String deletebtn = "delete from "+devds+".sm_appbutnregister where appid='"+pk_appregister+"'";
                    pstmt = getConn().prepareStatement(deletebtn);
                    textArea.appendText("\t"+deletebtn+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String deletereg = "delete from "+devds+".sm_appregister where code='"+code+"'";
                    pstmt = getConn().prepareStatement(deletereg);
                    textArea.appendText("\t"+deletereg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addreg = "insert into "+devds+".sm_appregister select * from "+jzkds+".sm_appregister where code='"+code+"'";
                    String addbtn = "insert into "+devds+".sm_appbutnregister select * from "+jzkds+".sm_appbutnregister where appid='"+pk_appregister+"'";
                    String addpage = "insert into "+devds+".sm_apppage select * from "+jzkds+".sm_apppage where parent_id='"+pk_appregister+"'";
                    pstmt = getConn().prepareStatement(addreg);
                    textArea.appendText("\t"+addreg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addbtn);
                    textArea.appendText("\t"+addbtn+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addpage);
                    textArea.appendText("\t"+addpage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    rs.close();
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                //处理menuitem
                textArea.appendText(System.lineSeparator()+System.lineSeparator()+"开始处理menuitem"+System.lineSeparator());
                //textArea.setText(log.toString());
                try {
                    String deletepage = "delete from "+devds+".sm_appmenuitem where menuitemcode='"+code+"'";
                    pstmt = getConn().prepareStatement(deletepage);
                    textArea.appendText("\t"+deletepage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addreg = "insert into "+devds+".sm_appmenuitem select * from "+jzkds+".sm_appmenuitem where menuitemcode='"+code+"'";
                    pstmt = getConn().prepareStatement(addreg);
                    textArea.appendText("\t"+addreg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                //处理pubpagetemplet
                textArea.appendText(System.lineSeparator()+System.lineSeparator()+"开始处理pubpagetemplet"+System.lineSeparator());
                //textArea.setText(log.toString());
                try {
                    String querypage = "select * from pub_page_templet where appcode='"+code+"' and dr=0";
                    pstmt = getConn().prepareStatement(querypage);
                    textArea.appendText("\t"+querypage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    ResultSet rs = pstmt.executeQuery();
                    String pk_page_templet = null;
                    while (rs.next()) {
                        pk_page_templet= rs.getString("pk_page_templet");
                        System.out.println(pk_page_templet);
                    }

                    String queryarea = "select * from pub_area where templetid='"+pk_page_templet+"'";
                    pstmt = getConn().prepareStatement(queryarea);
                    textArea.appendText("\t"+queryarea+System.lineSeparator());
                    //textArea.setText(log.toString());
                    rs = pstmt.executeQuery();
                    List<String> areaids = new ArrayList<>();
                    while (rs.next()) {
                        areaids.add(rs.getString("pk_area"));
                    }
                    String delqpro = "delete from "+devds+".pub_query_property where areaid in ('"+String.join("','",areaids)+"')";
                    String delfpro = "delete from "+devds+".pub_form_property where areaid in ('"+String.join("','",areaids)+"')";
                    String delqscheme = "delete from "+devds+".pub_ncc_queryscheme where areaid in ('"+String.join("','",areaids)+"')";
                    pstmt = getConn().prepareStatement(delqpro);
                    textArea.appendText("\t"+delqpro+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(delfpro);
                    textArea.appendText("\t"+delfpro+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(delqscheme);
                    textArea.appendText("\t"+delqscheme+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addqpro = "insert into "+devds+".pub_query_property select * from "+jzkds+".pub_query_property where areaid in ('"+String.join("','",areaids)+"')";
                    String addfpro = "insert into "+devds+".pub_form_property select * from "+jzkds+".pub_form_property where areaid in ('"+String.join("','",areaids)+"')";
                    String addscheme = "insert into "+devds+".pub_ncc_queryscheme select * from "+jzkds+".pub_ncc_queryscheme where areaid in ('"+String.join("','",areaids)+"')";
                    pstmt = getConn().prepareStatement(addqpro);
                    textArea.appendText("\t"+addqpro+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addfpro);
                    textArea.appendText("\t"+addfpro+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addscheme);
                    textArea.appendText("\t"+addscheme+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();

                    String delarea = "delete from "+devds+".pub_area where templetid='"+pk_page_templet+"'";
                    pstmt = getConn().prepareStatement(delarea);
                    textArea.appendText("\t"+delarea+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addarea = "insert into "+devds+".pub_area select * from "+jzkds+".pub_area where templetid='"+pk_page_templet+"'";
                    pstmt = getConn().prepareStatement(addarea);
                    textArea.appendText("\t"+addarea+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();

                    String delpage = "delete from "+devds+".pub_page_templet where pk_page_templet='"+pk_page_templet+"'";
                    pstmt = getConn().prepareStatement(delpage);
                    textArea.appendText("\t"+delpage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addpage = "insert into "+devds+".pub_page_templet select * from "+jzkds+".pub_page_templet where pk_page_templet='"+pk_page_templet+"'";
                    pstmt = getConn().prepareStatement(addpage);
                    textArea.appendText("\t"+addpage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    rs.close();
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                //处理pub_systemplet_base
                textArea.appendText(System.lineSeparator()+System.lineSeparator()+"开始处理pub_systemplet_base"+System.lineSeparator());
                //textArea.setText(log.toString());
                try {
                    String deletepage = "delete from "+devds+".pub_systemplate_base where appcode='"+code+"'";
                    pstmt = getConn().prepareStatement(deletepage);
                    textArea.appendText("\t"+deletepage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String addreg = "insert into "+devds+".pub_systemplate_base select * from "+jzkds+".pub_systemplate_base where appcode='"+code+"'";
                    pstmt = getConn().prepareStatement(addreg);
                    textArea.appendText("\t"+addreg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                //处理print
                textArea.appendText(System.lineSeparator()+System.lineSeparator()+"开始处理print"+System.lineSeparator());
                //textArea.setText(log.toString());
                try {
                    String querypage = "select * from PUB_PRINT_TEMPLATE where appcode='"+code+"' and dr=0";
                    pstmt = getConn().prepareStatement(querypage);
                    textArea.appendText("\t"+querypage+System.lineSeparator());
                    //textArea.setText(log.toString());
                    ResultSet rs = pstmt.executeQuery();
                    String ctemplateid = null;
                    while (rs.next()) {
                        ctemplateid= rs.getString("ctemplateid");
                        System.out.println(ctemplateid);
                    }
                    String delds = "delete from "+devds+".pub_print_datasource where ctemplateid='"+ctemplateid+"'";
                    pstmt = getConn().prepareStatement(delds);
                    textArea.appendText("\t"+delds+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String delline = "delete from "+devds+".pub_print_line where ctemplateid='"+ctemplateid+"'";
                    pstmt = getConn().prepareStatement(delline);
                    textArea.appendText("\t"+delline+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String delcell = "delete from "+devds+".pub_print_cell where ctemplateid='"+ctemplateid+"'";
                    pstmt = getConn().prepareStatement(delcell);
                    textArea.appendText("\t"+delcell+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String delvar = "delete from "+devds+".pub_print_variable where ctemplateid='"+ctemplateid+"'";
                    pstmt = getConn().prepareStatement(delvar);
                    textArea.appendText("\t"+delvar+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    String delreg = "delete from "+devds+".pub_print_region where ctemplateid='"+ctemplateid+"'";
                    pstmt = getConn().prepareStatement(delreg);
                    textArea.appendText("\t"+delreg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();

                    String addds = "insert into "+devds+".pub_print_datasource select * from "+jzkds+".pub_print_datasource where ctemplateid='"+ctemplateid+"'";
                    String addline = "insert into "+devds+".pub_print_line select * from "+jzkds+".pub_print_line where ctemplateid='"+ctemplateid+"'";
                    String addcell = "insert into "+devds+".pub_print_cell select * from "+jzkds+".pub_print_cell where ctemplateid='"+ctemplateid+"'";
                    String addvar = "insert into "+devds+".pub_print_variable select * from "+jzkds+".pub_print_variable where ctemplateid='"+ctemplateid+"'";
                    String addreg = "insert into "+devds+".pub_print_region select * from "+jzkds+".pub_print_region where ctemplateid='"+ctemplateid+"'";
                    String addtemp = "insert into "+devds+".PUB_PRINT_TEMPLATE select * from "+jzkds+".PUB_PRINT_TEMPLATE where appcode='"+code+"' and dr=0";
                    pstmt = getConn().prepareStatement(addds);
                    textArea.appendText("\t"+addds+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addline);
                    textArea.appendText("\t"+addline+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addcell);
                    textArea.appendText("\t"+addcell+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addvar);
                    textArea.appendText("\t"+addvar+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addreg);
                    textArea.appendText("\t"+addreg+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    pstmt = getConn().prepareStatement(addtemp);
                    textArea.appendText("\t"+addtemp+System.lineSeparator());
                    //textArea.setText(log.toString());
                    pstmt.execute();
                    rs.close();
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            });
            dialog.showAndWait();
        }


    }

    private static synchronized Connection getConn(){
        if(conn == null){
            try {
                Class.forName(driver);
                conn = DriverManager.getConnection(url, username, password);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return conn;
    }
}
